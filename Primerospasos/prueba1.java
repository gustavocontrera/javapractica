
/*Pongo este ejemplo en Java para que se vea que es mucho mas simple que en pseint, es decir que
que solo hace falta el nombre de la funcion y luego en el cuerpo en este caso solo la palabra
reservada "return"*/
public class prueba1 {


	public static int sumarNumeros (int numero1, int numero2) {
					return numero1 + numero2;
		}


	public static void main(String[] args) {

	int sumando1 = 40;
	System.out.println("Sumando 1: "+sumando1);
	int sumando2 = 80;
	System.out.println("Sumando 2: "+sumando2);
	int resultado = sumarNumeros(sumando1, sumando2);
	System.out.println("Resultado: "+resultado);

	}

} //final de la clase prueba1
/*
--------------------------------------------------------
Ejemplo en Javascript:

//DefiniciÃ³n de la funciÃ³n
	function suma_y_muestra(primerNumero, segundoNumero) {
	var resultado = primerNumero + segundoNumero;
	alert("El resultado es " + resultado);
	}

//DeclaraciÃ³n de las variables
	var numero1 = 3;
	var numero2 = 5;

//Llamada a la funciÃ³n
	suma_y_muestra(numero1, numero2);

-------------------------------------------------------------

//Otro ejemplo en javascript todavia hay que mejorarlo:

function calculaPrecioTotal(precio, porcentajeImpuestos) {
  var gastosEnvio = 10;
  var precioConImpuestos = (1 + porcentajeImpuestos/100) * precio;
  var precioTotal = precioConImpuestos + gastosEnvio;
  alert("El precio total es: " + precioTotal);
  return precioTotal;
}
 
var primerPrecioTotal = calculaPrecioTotal(23.34, 16
var otroPrecioTotal = calculaPrecioTotal(15.20, 4);


------------------------------------------------------

	//Ejemplo en Pseint

	// funcion que no recibe ni devuelve nada
	Funcion Saludar
		Escribir "Hola mundo!";
	FinFuncion

	//res, prinum, secnum son variables locales, significa que solo sirven dentro de la funcion
	Funcion res <- CalcularSumaDosNum(prinum,secnum)
		res <- prinum + secnum;
	FinFuncion

	Funcion res <- CalcularRestaDosNum(prinum,secnum)
		res <- prinum - secnum;
	FinFuncion

	Funcion LineasDeSeparacion
		Escribir "                                     ";
		Escribir "-------------------------------------";
		Escribir "-------------------------------------";
		Escribir "                                     ";
	FinFuncion

	Funcion res <- PedirDosNum(num1 Por referencia ,num2 Por Referencia)
		Escribir "Ingrese el primer numero: ";
		Leer num1;
		Escribir "Ingrese el segundo numero: ";
		Leer num2;
	FinFuncion


	//*****************************************************************************************
	// proceso principal, que invoca a las funciones antes declaradas
	Algoritmo FuncionPrueba02

		//verifico que las variables res, prinum, secnum locales de las funciones no son las mismas que estas
		//Al final del algoritmo voy a verificar que no cambiaron
		res<-0;
		prinum<-0;
		secnum<-0;

		//Llamada a la funcion Saludar sin argumentos - Funcion que no recibe ni devuelve nada
		Escribir "Llamada a la funcion Saludar:";
		Saludar; // como no recibe argumentos pueden omitirse los paréntesis vacios

		LineasDeSeparacion;

		//Llamada a la Funcion CalcularSumaDosNum con numeros predefinidos como argumento
		Escribir "Llamada a la funcion CalcularSumaDosNum: ";
		Escribir "La suma de 6 mas 4 es: ", CalcularSumaDosNum(6,4);

		LineasDeSeparacion;

		//Llamada a la Funcion CalcularSumaDosNum con variables
		Escribir "Llamada a la funcion CalcularSumaDosNum (Con variables): ";
		Escribir PedirDosNum(alfa,beta);

		Escribir "La suma de los numeros ingresados es: ", CalcularSumaDosNum(alfa,beta);

		LineasDeSeparacion;

		//Funcion CalcularRestaDosNum con variables
		Escribir "Llamada a la Funcion CalcularRestaDosNum: ";
		Escribir "Ingrese el kilometro de dolores (donde se encuentra usted ahora): ";
		Leer num3;
		Escribir "Ingrese el kilometro de Mardel: ";
		Leer num4;
		Escribir "Le queda por recorrer: ", CalcularRestaDosNum(num4,num3), " Kilmetros";

		LineasDeSeparacion;

		//Funcion CalcularRestaDosNum con variables
		Escribir "Llamada a la Funcion CalcularRestaDosNum: ";
		Escribir "Ingrese el año en que estamos actualmente: ";
		Leer num5;
		Escribir "Ingrese el año en que nacio: ";
		Leer num6;
		Escribir "Usted tiene: ", CalcularRestaDosNum(num5,num6), " años";

		LineasDeSeparacion;

		Escribir "Aca se verifica que las variables que inicialice en 0 dentro del algoritmo (res, prinum, secnum) ";
		Escribir "realmente no se modificaron ya que no son las mismas que las de las funciones: ";
		Escribir res, prinum, secnum;

	FinAlgoritmo
*/
